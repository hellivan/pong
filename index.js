'use strict';

var Promise = require('bluebird');
var dbg = require('debug');

module.exports.name = 'pong';

module.exports.pong = function(){
    return Promise.resolve()
        .then(()=>{
            dbg.debug("ping");
        });
}
